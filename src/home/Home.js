import "./Home.css";
import "./Home.mobile.css";
import "./Home.tablet.css";
import "./Home.desktop.css";
import NavBar from "../navbar/NavBar";
import { Outlet } from "react-router-dom";
import { BsLinkedin } from "react-icons/bs";
import { RiGitlabFill } from "react-icons/ri";
import { BsEnvelopePaperHeartFill } from "react-icons/bs";
import { Tooltip as ReactTooltip } from "react-tooltip";

function Home() {
    
    const womanOnComputerImg= require("../assets/woman-on-computer.png");
    

    return <div id="home">
        <div className="home-container">
            <div className="top-image">
                <img src={womanOnComputerImg} alt="Woman typing on the computer."/>
            </div>
            <div className="top-title">
                <div className="name">Breisa Moralde</div>
                <div className="occupation">Full Stack Software Engineer</div>
                <div className="socials">
                    <ul>
                        <li>
                            <a target="_blank" href="https://www.linkedin.com/in/breisa-m-2a17b135/">
                                <BsLinkedin id="linkedin-link" data-tooltip-content="LinkedIn Profile"/>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://gitlab.com/zeitgeist">
                                <RiGitlabFill id="gitlab-link" data-tooltip-content="GitLab Profile"/>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://drive.google.com/file/d/1ugBPDozXsnuKjDQVQpAg8CsOs2hreg91/view?usp=sharing">
                                <BsEnvelopePaperHeartFill id="cv-link" data-tooltip-content="Resume"/>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <NavBar/>

            <Outlet />

            <ReactTooltip anchorId="linkedin-link" />
            <ReactTooltip anchorId="gitlab-link" />
            <ReactTooltip anchorId="cv-link" />
        </div>
    </div>
}

export default Home;