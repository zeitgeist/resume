const Education = {

    getExperiences: function() {
        return [
            {
                "school": "Xavier University",
                "location": "Cagayan de oro City",
                "startDate": new Date("2007-01-01"),
                "endDate": new Date("2011-11-01"),
                "degree": "Bachelor of Science in Computer Science"
            }
        ];
    }

};

export default Education;