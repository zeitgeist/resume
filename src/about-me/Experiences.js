const Experiences = {

    getExperiences: function() {
        return [
            {
                "companyName": "Seven Hills Technology",
                "title": "International Contractor",
                "location": "Cincinatti, Ohio, United States",
                "startDate": new Date("2024-10-07"),
                "endDate": new Date(),
                "responsibilities": [
                    "- Upgraded the Angular application to enhance performance and maintain compatibility with modern standards.",
                    "- Implemented new features to extend the app's functionality and optimized several endpoints, improving overall efficiency and responsiveness."
                ]
            },
            {
                "companyName": "Synacy",
                "title": "Senior Software Engineer",
                "location": "Cebu City, Philippines",
                "startDate": new Date("2016-01-01"),
                "endDate": new Date(),
                "responsibilities": [
                    "- Utilized Java, Grails, and Angular to build scalable, responsive web applications and integrated them with Springboot and Gradle for efficient deployment to AWS.",
                    "- Helped in assessing technical abilities of software developers who are underwent the hiring process.",
                    "- Collaborated with cross-functional teams to define and prioritize project requirements.",
                    "- Worked closely with QA team to ensure applications met all requirements and standards.",
                    "- Utilized agile development methodologies to deliver high-quality software on tight deadlines.",
                    "- Mentored junior developers on best practices in software development.",
                    "- Upgraded legacy applications to the latest versions of Java and Angular."
                ]
            },
            {
                "companyName": "Orange and Bronze",
                "title": "Associate Software Engineer",
                "location": "Makati City, Philippines",
                "startDate": new Date("2011-01-01"),
                "endDate": new Date("2016-05-01"),
                "responsibilities": [
                    "- Helped develop, test and deploy web applications for the various clients of the company.",
                    "- Main languages used were Java and Groovy on Grails."
                ]
            }
        ];
    }

};

export default Experiences;