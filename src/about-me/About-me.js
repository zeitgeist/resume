import { format } from "date-fns";
import { useEffect, useState } from "react";
import Experiences from "./Experiences";
import Education from "./Education";
import "./About-me.css";
import "./About-me.mobile.css";
import "./About-me.desktop.css";
import "./About-me.large-desktop.css";
import "./About-me.tablet.css";
import TechStack from "../tech-stack/TechStack";

function AboutMe() {

    const [experiences, setExperiences] = useState([]);
    const [education, setEducation] = useState([]);

    const workingStartDate = new Date("2011-12-01");    
    const [yearsOfExp, setYearsOfExp] = useState(0);

    useEffect(() => {
        setExperiences(Experiences.getExperiences());
        setEducation(Education.getExperiences());

        const today = new Date();
        const diffInYears = today.getFullYear() - workingStartDate.getFullYear();
        setYearsOfExp(diffInYears);
    }, []);

    function formatDateMMYYYY(date) {
        return format(date, "MMM yyyy");
    }

    function formatDate(date) {
        if(isPresent(date)) {
            return "present";
        }

        return format(date, "MMM yyyy");
    }

    function isPresent(date) {
        const today = new Date();
        return today.getFullYear() === date.getFullYear() && today.getMonth() === date.getMonth();
    }

    return <div id="about-me">

        <div className="about-me-wrapper">
            <div className="expertise">
                    Over {yearsOfExp} years of experience as a software developer, with a strong background in Java, Angular, and AWS.
Proven track record of delivering high-quality web applications on time and within budget.
Skilled in working collaboratively in fast-paced, team-oriented environments.
Proficient in a variety of development tools, including, Visual Studio, IntelliJ, and Git.
            </div>

            <div className="experience-container">
                <div className="title">experience</div>

                <div className="experiences">
                    {experiences.map((experience, expIndex) => {
                        return <div key={expIndex} className="experience">
                            <div className="name"> {experience.companyName} </div>
                            <div className="job-title"> ({experience.title}) </div>
                            <div className="location"> {experience.location} </div>
                            <div className="date">   
                                {formatDateMMYYYY(experience.startDate)} - {formatDate(experience.endDate)}
                            </div>
                            <ul className="responsibilities">
                                {experience.responsibilities.map((responsibility, respIndex) => {
                                    return <li key={respIndex} className="responsibility">{responsibility}</li>
                                })}
                            </ul>
                        </div>
                    })}
                </div>
            </div>

            <div className="experience-container">
                <div className="title">education</div>

                <div className="experiences">
                    {education.map((experience, expIndex) => {
                        return <div key={expIndex} className="experience">
                            <div className="name"> {experience.school} </div>
                            <div className="location"> {experience.location} </div>
                            <div className="date">   
                                {formatDate(experience.startDate)} - {formatDate(experience.endDate)}
                            </div>
                            <ul className="responsibilites">
                                <li className="responsibility">{experience.degree}</li>
                            </ul>
                        </div>
                    })}
                </div>
            </div>

            <div className="experience-container">
                <div className="title">Tech Stack</div>
                <TechStack/>
            </div>
        </div>
    </div>
}

export default AboutMe;