import './App.css';
import Home from './home/Home';
import AboutMe from './about-me/About-me';
import ContactMe from './contact-me/Contact-me';
import Projects from './projects/Projects';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {

  return (
    <div className="resume-app">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />}>
            <Route index element={<AboutMe />} />
            <Route path="about-me" element={<AboutMe />} />
            <Route path="projects" element={<Projects />} />
            <Route path="contact-me" element={<ContactMe />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
