import './NavBar.css';
import './NavBar.mobile.css';
import './NavBar.tablet.css';
import './NavBar.desktop.css';
import { NavLink } from 'react-router-dom';
import React, { useState } from 'react';

const NavBar = () => {
    const [activeTab, setActiveTab] = useState('aboutMe');

    const handleTabClick = (tabName) => {
        setActiveTab(tabName);
    };

    return <nav className="navbar">
        <ul>
            <li>
                <NavLink to="/about-me" 
                className={activeTab === 'aboutMe' ? 'active' : ''}
                onClick={() => handleTabClick('aboutMe')}
                >About Me</NavLink>
            </li>
            <li>
                <NavLink to="/projects" 
                className={activeTab === 'projects' ? 'active' : ''}
                onClick={() => handleTabClick('projects')}
                >Projects</NavLink>
            </li>
            <li>
                <NavLink to="/contact-me" 
                className={activeTab === 'contactMe' ? 'active' : ''}
                onClick={() => handleTabClick('contactMe')}
                >Contact Me</NavLink>
            </li>
        </ul>
    </nav>;
}

export default NavBar;