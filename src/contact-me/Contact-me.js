
import { useRef, useState } from "react";
import EmailSender from "../email-sender/Email-Sender";
import "./Contact-me.css";
import "./Contact-me.mobile.css";
import "./Contact-me.desktop.css";
import "./Contact-me.large-desktop.css";
import "./Contact-me.tablet.css";

function ContactMe() {
    
    const SUCCESS_MESSAGE = "Got it! I'll get back to you soon!";
    const SUCCESS_MESSAGE_CLASS = "success-message";

    const ERROR_MESSAGE = "An error occurred while \n sending the message. \n Please try again later."
    const ERROR_MESSAGE_CLASS = "error-message";

    const [isLoading, setIsLoading] = useState(false);
    const [showConfirmMessage, setShowConfirmMessage] = useState(false);
    const [confirmMessageClass, setConfirmMessageClass] = useState(ERROR_MESSAGE_CLASS)
    const [confirmMessage, setConfirmMessage] = useState(ERROR_MESSAGE);

    const form = useRef();

    function submitForm(event) {
        setIsLoading(true);
        setShowConfirmMessage(false);
        event.preventDefault();

        EmailSender.sendEmail(form)
        .then((result) => {
            event.target.reset();
            setConfirmMessage(SUCCESS_MESSAGE);
            setConfirmMessageClass(SUCCESS_MESSAGE_CLASS);
        }, (error) => {
            setConfirmMessage(ERROR_MESSAGE);
            setConfirmMessageClass(ERROR_MESSAGE_CLASS);
        }).finally(() => {
            setShowConfirmMessage(true);
            setIsLoading(false);
        });
    }

    return <div id="contact-me">
        {
            showConfirmMessage && (
                <div className={ "confirm-message " + confirmMessageClass }>
                    {confirmMessage}
                </div>
            )
        }
        <form ref={form} className="contact-me-form" onSubmit={submitForm}>
            <div className="form-group">
                <input type="text" className="name-input-box" name="name" placeholder="name" required></input>
            </div>

            <div className="form-group">
                <input type="email"className="email-input-box" name="recipient_email" placeholder="email address" required></input>
            </div>

            <div className="form-group">
                <textarea type="text" className="message-input-box" name="message" placeholder="message" rows="5" required></textarea>
            </div>

            <div className="form-group submit">
                <input type="submit" className="submit-button" value="Send" disabled={isLoading}/>
            </div>
        </form>
    </div>
}

export default ContactMe;