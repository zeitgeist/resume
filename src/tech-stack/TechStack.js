import { DiJava, DiGrails, DiGroovy, DiAngularSimple, DiNodejsSmall } from "react-icons/di";
import { SiJavascript } from "react-icons/si";
import { FaAws, FaGitSquare } from "react-icons/fa";
import { RiReactjsFill } from "react-icons/ri";
import { Tooltip as ReactTooltip } from "react-tooltip";

import "./TechStack.css";

function TechStack() {

    const ICON_SIZE=50;

    return <div className="tech-stack">
        <div>
            <DiJava id="tech-stack-java" data-tooltip-content="Java" size={ICON_SIZE}/>
        </div>
        
        <div>
            <SiJavascript id="tech-stack-javascript" data-tooltip-content="Javascript" size={ICON_SIZE}/>
        </div>

        <div>
            <DiGroovy id="tech-stack-groovy" data-tooltip-content="Groovy" size={ICON_SIZE}/>
        </div>
        
        <div>
            <DiAngularSimple id="tech-stack-angular" data-tooltip-content="Angular" size={ICON_SIZE}/>
        </div>

        <div>
            <DiGrails id="tech-stack-grails" data-tooltip-content="Grails" size={ICON_SIZE}/>
        </div>

        <div>
            <FaAws id="tech-stack-aws" data-tooltip-content="AWS" size={ICON_SIZE}/>
        </div>

        <div>
            <RiReactjsFill id="tech-stack-react" data-tooltip-content="React" size={ICON_SIZE}/>
        </div>

        <div>
            <DiNodejsSmall id="tech-stack-node" data-tooltip-content="NodeJS" size={ICON_SIZE}/>
        </div>

        <div>
            <FaGitSquare id="tech-stack-git" data-tooltip-content="Git" size={ICON_SIZE}/>
        </div>

        <ReactTooltip anchorId="tech-stack-java" />
        <ReactTooltip anchorId="tech-stack-javascript" />
        <ReactTooltip anchorId="tech-stack-groovy" />
        <ReactTooltip anchorId="tech-stack-angular" />
        <ReactTooltip anchorId="tech-stack-grails" />
        <ReactTooltip anchorId="tech-stack-aws" />
        <ReactTooltip anchorId="tech-stack-react" />
        <ReactTooltip anchorId="tech-stack-node" />
        <ReactTooltip anchorId="tech-stack-git" />
    </div>

}

export default TechStack;