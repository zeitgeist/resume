import { useEffect, useState } from "react";
import ProjectDetails from "./Project-details";
import "./Projects.css";
import "./Projects.mobile.css";
import "./Projects.tablet.css";
import "./Projects.desktop.css";
import "./Projects.large-desktop.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { AiOutlineEye } from "react-icons/ai";

function Projects() {

    const [projects, setProjects] = useState([]);
    const images = require.context('../assets/', true);

    useEffect(() => {
        setProjects(ProjectDetails.getProjectDetails());
    }, []);


    return <div id="projects">
        <div className="project"> 
        I have a deep passion for software development, 
        and my journey in this field has been marked by the creation of numerous projects. 
        These endeavors have allowed me to continually expand my expertise in web development. 
        Below, you'll find a collection of projects I've diligently worked on throughout the years, 
        encompassing both professional and personal endeavors.
        </div>

        {projects.map((project, index) => {
            const imgSrc= images(`./${project.image}`);
            
            return <div className="project" key={index}>
                <div className="project-image"> 
                    <a href={project.link} target="_blank" rel="noreferrer">
                        <LazyLoadImage
                        className="project-image"
                        alt="project preview"
                        effect="blur"
                        src={imgSrc} />
                    </a>
                </div>
                <div className="project-name"><a href={project.link} target="_blank" rel="noreferrer"> {project.name}</a> ({project.projectType})</div>
                <div className="project-description">{project.description}</div>
                <div className="project-tech-used">
                    {project.techUsed.map((usedTech, idx) => {
                        return <div key={idx}>{usedTech}</div>
                    })}
                </div>
                
                <div className="project-source-code">
                    {project.frontEnd && (
                    <a href={project.frontEnd} target="_blank" rel="noreferrer">
                        <AiOutlineEye/> <span>front end code </span>
                    </a>
                    )}

                    {project.backEnd && (
                    <a href={project.backEnd} target="_blank" rel="noreferrer">
                        <AiOutlineEye/> <span>back end code</span>
                    </a>
                    )} 
                </div>
                
                
            </div>
        })}
    </div>
}

export default Projects;