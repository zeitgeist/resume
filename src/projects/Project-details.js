const ProjectDetails = {

    getProjectDetails: function() {
        return [
            {
                "name": "Monopond Public Site",
                "link": "https://monopond.com",
                "image": "monopond-public-site.png",
                "description": "Monopond provides businesses with reliable, affordable, and secure Email to SMS, SMS Broadcast, Online Fax, and Fax Broadcast services.",
                "techUsed": [ "HTML", "CSS", "Angular", "Java", "Help Scout" ],
                "projectType": "Work Related",
                "frontEnd": null,
                "backEnd": null
            },
            {
                "name": "Monopond Help Center",
                "link": "https://support.monopond.com",
                "image": "monopond-help-center.png",
                "description": "Monopond help center is a website that assists monopond customers with their concerns regarding the monopond platform.",
                "techUsed": [ "HTML", "CSS", "Javascript" ],
                "projectType": "Work Related",
                "frontEnd": null,
                "backEnd": null
            },
            {
                "name": "Filebun",
                "link": "https://gitlab.com/sky-chasers/linkgenie",
                "image": "filebun.png",
                "description": "A website that can summarize pdf files with the use of AI. It's a personal project I created to practice my skills in Java, Angular and AI.",
                "techUsed": [ "HTML", "CSS", "Angular", "Java", "Vercel", "Digital Ocean", "Hugging Face", "Paypal API"],
                "projectType": "Personal Project",
                "frontEnd": 'https://gitlab.com/sky-chasers/linkgenie',
                "backEnd": 'https://gitlab.com/sky-chasers/linkgenie'
            },
            {
                "name": "Landau Landing Page",
                "link": "https://landau.vercel.app",
                "image": "landau.png",
                "description": "A responsive website for Dr. Alex Landau.",
                "techUsed": [ "HTML", "CSS", "Angular", "Vercel" ],
                "projectType": "Freelance",
                "frontEnd": null,
                "backEnd": null
            },
            {
                "name": "Yamada Tarou Landing Page",
                "link": "https://yamadatarou.vercel.app",
                "image": "yamada.png",
                "description": "A responsive website for Yamada Tarou.",
                "techUsed": [ "HTML", "CSS", "Angular", "Vercel" ],
                "projectType": "Freelance",
                "frontEnd": null,
                "backEnd": null
            },
            {
                "name": "D'flex Spa Therapy Landing page",
                "link": "https://dflexspa.vercel.app/",
                "image": "dflexspa.png",
                "description": "A responsive website for Dflex Spa Therapy.",
                "techUsed": [ "HTML", "CSS", "Angular", "Vercel" ],
                "projectType": "Freelance",
                "frontEnd": "https://gitlab.com/zeitgeist/dflexspa",
                "backEnd": null
            },
            {
                "name": "Chada",
                "link": "https://chada.vercel.app",
                "image": "cdo-yp.png",
                "description": "Commissioned work for CDO coffee crawlers.",
                "techUsed": [ "HTML", "CSS", "Angular", "Java", "Vercel", "Docker", "Render" ],
                "projectType": "Freelance",
                "frontEnd": "https://gitlab.com/sky-chasers/cdoyp",
                "backEnd": "https://gitlab.com/sky-chasers/cdoyp-be"
            },
            {
                "name": "Pepper Potts Discord Bot",
                "link": "https://gitlab.com/sky-chasers/gherdith",
                "image": "pepper.png",
                "description": "Pepper is a discord bot designed to assist the guild leader with his various responsibilities. It also provides the latest news about the game (tree of savior) pulled from various sources with the korean news auto translated to english. The project is created with Java, Postgres, Springboot, Gradle and deployed in Heroku. It makes use of Javacord API to communicate with Discord. It also makes use of Naver's Papago API to auto translate korean news to english.",
                "techUsed": [ "Java", "Javacord API", "Google Sheets API", "Papago API", "Heroku" ],
                "projectType": "Personal Project",
                "frontEnd": null,
                "backEnd": "https://gitlab.com/sky-chasers/gherdith"
            },
            {
                "name": "Cucurbit",
                "link": "https://gitlab.com/zeitgeist/cucurbit",
                "image": "cucurbit.png",
                "description": "An app that contains information about various recipes in a game called Stardew Valley. The project's backend is created with Java, Postgres, Springboot, Gradle and is deployed in Heroku. The frontend is made with Angular and deployed in netlify.",
                "techUsed": [ "HTML", "CSS", "Angular", "Java", "Heroku" ],
                "projectType": "Personal Project",
                "frontEnd": "https://gitlab.com/zeitgeist/cucurbit/-/tree/main/cucurbit-frontend/cucurbit",
                "backEnd": "https://gitlab.com/zeitgeist/cucurbit/-/tree/main/cucurbit-backend"
            },
            {
                "name": "My Portfolio",
                "link": "https://breisa.vercel.app/",
                "image": "portfolio.png",
                "description": "A small website I created to showcase my skills and projects over the years.",                
                "techUsed": [ "HTML", "CSS", "ReactJS", "Vercel"],
                "projectType": "Personal Project",
                "frontEnd": "https://gitlab.com/zeitgeist/resume",
                "backEnd": null
            },
            {
                "name": "Adxn",
                "link": "https://gitlab.com/sky-chasers/zenzen-backend",
                "image": "adxn.png",
                "description": " I am currently developing Adxn, a classified ad website inspired by craigslist, as a personal project to gain experience with ReactJS and NodeJS. This project allows me to apply my web development skills in my spare time and learn new technologies. The frontend is made with ReactJS, backend is made with NodeJS, supabase was utilized for the database. It's currently deployed in vercel and render.",
                "techUsed": [ "HTML", "CSS", "ReactJS", "NodeJS", "Vercel", "Render" ],
                "projectType": "Personal Project",
                "frontEnd": "https://gitlab.com/sky-chasers/zenzen-ui",
                "backEnd": "https://gitlab.com/sky-chasers/zenzen-backend"
            },
            {
                "name": "Prettify",
                "link": "https://prettify.vercel.app/",
                "image": "prettify.png",
                "description": "I developed prettify to practice my skills in reactJS. It's a website that can turn ugly json into a human readable, friendly format.",                
                "techUsed": [ "HTML", "CSS", "ReactJS", "Vercel" ],
                "projectType": "Personal Project",
                "frontEnd": "https://gitlab.com/zeitgeist/prettify",
                "backEnd": null
            }
        ]
    }

};

export default ProjectDetails;